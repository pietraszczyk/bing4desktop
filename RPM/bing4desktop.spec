#
# spec file for package bing4desktop
#
# Copyright (c) 2020 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

%define unmangled_version 0.240209-0
Name:       bing4desktop
Version:	0.240209
Release:    0
Summary:	Beautify your desktop with photos from Bing search engine
License:	copyleft Creative Commons Attribution-Share Alike 4.0 International (CC BY-SA 4.0)
URL:		https://prymula.ct8.pl
Source0:	%{name}-%{unmangled_version}.tar.gz
%if 0%{?suse_version}>=150000
BuildRequires:	python3, python-Pillow, python3-requests, python3-tk
Requires:       python3, python-Pillow, python3-requests, python3-tk
%endif
%if 0%{?fedora}>=36
BuildRequires:	python3, python3-pillow-tk, python3-requests, python3-tkinter, python3-pystray
Requires:		python3, python3-pillow-tk, python3-requests, python3-tkinter, python3-pystray
%endif
BuildArch:	noarch

%description

%prep
%setup -n %{name}-%{unmangled_version} -n %{name}-%{unmangled_version}

%build

%install

mkdir -p %{buildroot}/usr
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}/usr/share
mkdir -p %{buildroot}/usr/share/bing4desktop
mkdir -p %{buildroot}/usr/share/applications
mkdir -p %{buildroot}/etc
mkdir -p %{buildroot}/etc/xdg
mkdir -p %{buildroot}/etc/xdg/autostart


install -m 0755 bing4desktop.py %{buildroot}/usr/bin
install -m 0644 bing.ico %{buildroot}/usr/share/bing4desktop
install -m 0644 bing.png %{buildroot}/usr/share/bing4desktop
install -m 0644 bing4desktop.desktop %{buildroot}/etc/xdg/autostart
install -m 0644 bing4desktop.desktop %{buildroot}/usr/share/applications

%files
%defattr(0755,root,root)
/usr/bin/bing4desktop.py
/usr/share/bing4desktop/
%defattr(0644,root,root)
/usr/share/bing4desktop/bing.ico
/usr/share/bing4desktop/bing.png
/etc/xdg/autostart/bing4desktop.desktop
/usr/share/applications/bing4desktop.desktop

%changelog

