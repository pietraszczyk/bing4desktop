﻿# bing4desktop

## Start

To download the latest version, type in terminal:

```
git clone https://gitlab.com/pietraszczyk/bing4desktop.git

# You still need to install dependencies to run:


# Fedora:

sudo dnf install python3-pillow-tk python3-requests python3-tkinter python3-pystray make


#OpenSUSE:


sudo zypper in python-Pillow python3-requests python3-tk make

# except that you will have to install the 'pystray' package via pip:

pip3 install pystray


# Ubuntu/Debian

sudo apt install python3-requests python3-tk python3-pil.imagetk python3-pystray make


#Arch:

sudo pacman -S python-pillow python-requests tk
```

source archive 'pystarce' should be downloaded from the [AUR] repository (https://aur.archlinux.org/packages/python-pystray) along with PKGBUILD, and build the package with the command 'makepkg'.



Running on a trial basis

For a trial run before installation, you can run the application with the command:

./bing4desktop.py



Installation on file system:

```
# Arch, Fedora and OpenSUSE:

sudo make install


# Debian/Ubuntu:

sudo make -f Makefile.debian install
```


## Finish

First startup - manual and after reloading the system - automatic

(Cinnamon) Menu->Preferences->Bing4Desktop


In addition to Ubuntu and Debian, you should allow applications to autostart in the 

(Cinnamon) Menu→Preferences→System Settings→Startup Programs.


Have a nice slide !




De installation:
```
#Fedora, Arch, OpenSUSE:

sudo make uninstall


#Ubuntu/Debian

sudo make -f Makefile.debian uninstall
```

