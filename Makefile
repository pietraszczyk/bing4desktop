prefix=/usr

install: bing4desktop.py
	mkdir -pm 0755 $(prefix)/bin
	mkdir -pm 0755 $(prefix)/share/bing4desktop
	#mkdir -pm 0755 /etc/xdg/autostart
	install -m 0755 bing4desktop.py $(prefix)/bin
	install -m 0644 bing.ico $(prefix)/share/bing4desktop
	install -m 0644 bing.png $(prefix)/share/bing4desktop
	install -m 0644 bing4desktop.desktop $(prefix)/share/applications
	#install -m 0644 bing4desktop.desktop /etc/xdg/autostart/
	$(shell ./copy_desktop.sh)
.PHONY: install

uninstall:
	rm $(prefix)/bin/bing4desktop.py
	rm $(prefix)/share/bing4desktop/bing.ico
	rm $(prefix)/share/bing4desktop/bing.png
	rm $(prefix)/share/applications/bing4desktop.desktop
	#rm /etc/xdg/autostart/bing4desktop.desktop
	rmdir $(prefix)/share/bing4desktop/
	$(shell ./remove_desktop.sh)
.PHONY: uninstall
