-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 1.0
Source: bing4desktop
Binary: bing4desktop
Architecture: all
Version: 0.240209-0
Maintainer: Przemysław R. Pietraszczyk <prymula76@outlook.com>
Homepage: http://pietraszczyk.smallhost.pl

DEBTRANSFORM-TAR: bing4desktop-0.240209-0.tar.gz
 
Standards-Version: 4.6.2.0
Build-Depends: debhelper-compat (>= 12), python3-all, dh-python, python3-requests, python3-tk, python3-pil.imagetk, python3-pystray, fakeroot
Package-List:
 bing4desktop deb x11 optional arch=all
Files:
 b31649bc23b2b7bca9ab70ea3410711a 128820 bing4desktop-0.240209-0.tar.gz
 29e7e7e60bc81891e3da3871b9eeb549 764 bing4desktop-0.240209-0.debian.tar.gz

-----BEGIN PGP SIGNATURE-----

mQENBGEf/c4BCAC2d4ymW0pRZV36qLtlt/WGb83kos5UX5UbwvkQrbUjAbOPOY8w
DT3M1neYOAPZx38924aPTFKbZPcz+rK/7Wcv1kzgfux5zXQJTLeqpkhUYAgXUg2p
oK9ZXsai82fOicTrClOyJLLYQ8C1vj8yUh7e3ERljEyp5Nxg/lY92rwcZ4WYR193
nGDInlDl5JlWUcLlk/RFnC5bB+T6ZZr5FBX/eDnKAPEl+N3MWpDs2JLDy7EUPhcG
U/60X0wuXHPTwMoNbB3ep/bWRxwEej0sFad5GXeCh7hKCroi/kLmLTDIxSD8lsRr
Y4H/8H2lBBsQq7bRL88N6ZDGCocPo11V83kFABEBAAG0NmhvbWU6cHJ6ZW0gT0JT
IFByb2plY3QgPGhvbWU6cHJ6ZW1AYnVpbGQub3BlbnN1c2Uub3JnPokBPgQTAQgA
KAUCYR/9zgIbAwUJBB6wAAYLCQgHAwIGFQgCCQoLBBYCAwECHgECF4AACgkQcXEc
NPcNefFEAAf+Kg4cs/C99kEIMv/PO85VW+p8rcTwCViOFUX5WrBhwsRyPAKrJPPG
KD6PdnvoplcNQwhQ8nRsRiiaiUtPcj8Bvi6EdPgSgXXObKKJtCyaAa1rURdlb9iW
CfvXA6oW+PGlii9BMnXOAtkeFIzZooO9oXd+wez66P+4VWFiFzxanLfLn/hwbWxQ
68rlO3QNGbrMeODBgwh9s83XGmAg47mn2fldTAmTbJDf9tMi1LhX2MmLyN9wjqsd
4IZ9JMri2JTnIhYfxWdKT9gsu7q0g8LqonaXMvlOEyHFRF1YQ0i7G6fFbvDcKCZN
W1Eja55FdN7smv5Eck6UTIIYyWPuAv45uohGBBMRAgAGBQJhH/3PAAoJEDswEbdr
nWUj4M0AniJ389dyvUjjgS91AcOb08E4moV9AJ9WDL3/7GYYUL/Azw43QucWPnXr
uw==
=+lfH
-----END PGP SIGNATURE-----
