#!/bin/sh

MYDIR=/home

for file in $(ls ${MYDIR}) ; do\
	if  [ $file == '..' ] ; then
		continue 
	fi
	cp bing4desktop.desktop /home/${file}/.local/share/applications
done
