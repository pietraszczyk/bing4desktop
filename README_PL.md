﻿## Start

Aby pobrać najnowszą wersję, wpisz w terminalu:

```
git clone https://gitlab.com/pietraszczyk/bing4desktop.git

# Do uruchomienia potrzebujesz jeszcze zainstalować zależności:


# Fedora:
sudo dnf install python3-pillow-tk python3-requests python3-tkinter python3-pystray make


# OpenSUSE:
sudo zypper in python-Pillow python3-requests python3-tk make

# z tym że pakiet ‘pystray’ będziesz musiał zainstalować przez pip:
pip3 install pystray


# Ubuntu:
sudo apt install python3-requests python3-tk python3-pil.imagetk python3-pystray make


#Arch:
pacman -S python-pillow python-requests tk

# archiwum źródłowe ‘pystarce’ należy pobrać z repozytorium [AUR]  (https://aur.archlinux.org/packages/ python-pystray) wraz z PKGBUILD, oraz zbudować pakiet poleceniem:

makepkg
```



Uruchomienie na próbę

Na próbę przed instalacją możesz uruchomić aplikację poleceniem:

./bing4desktop.py




Instalacja w systemie plików:

```
# Arch, Fedora oraz OpenSUSE:

make install


#cDebian/Ubuntu:

make -f Makefile.debian
```


## Finisz

Pierwsze uruchomienie – ręczne a po przeładowaniu systemu – automatyczne

(Cinnamon) Menu->Preferencje->Bing4Desktop


Oprócz Ubuntu i Debian, należy zezwolić na autostart aplikacji w 

(Cinnamon) Menu→Preferencje→Ustawienia Systemowe→Programy Startowe


Miłego slajowania !




De instalacja:

```
# Fedora, Arch, OpenSUSE:

sudo make uninstall

# Ubuntu/Debian

sudo make -f Makefile.debian uninstall
```


